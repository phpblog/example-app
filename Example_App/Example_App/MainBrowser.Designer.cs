﻿namespace Example_App
{
    partial class MainBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainBrowser));
            this.label1 = new System.Windows.Forms.Label();
            this.mnStrip = new System.Windows.Forms.MenuStrip();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnWebBrowser = new System.Windows.Forms.Button();
            this.lblClock = new System.Windows.Forms.Label();
            this.Clock = new System.Windows.Forms.Timer(this.components);
            this.btnFull = new System.Windows.Forms.Button();
            this.btnMini = new System.Windows.Forms.Button();
            this.btnYMp4D = new System.Windows.Forms.Button();
            this.btnMp = new System.Windows.Forms.Button();
            this.mnStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Algerian", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(311, 473);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(560, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Example Application by S.Nagy Zsolt";
            // 
            // mnStrip
            // 
            this.mnStrip.BackColor = System.Drawing.Color.Gray;
            this.mnStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.mnStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileToolStripMenuItem});
            this.mnStrip.Location = new System.Drawing.Point(9, 9);
            this.mnStrip.Name = "mnStrip";
            this.mnStrip.Size = new System.Drawing.Size(72, 28);
            this.mnStrip.TabIndex = 2;
            this.mnStrip.Text = "menuStrip1";
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.profileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editProfileToolStripMenuItem});
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(64, 24);
            this.profileToolStripMenuItem.Text = "Profile";
            // 
            // editProfileToolStripMenuItem
            // 
            this.editProfileToolStripMenuItem.Name = "editProfileToolStripMenuItem";
            this.editProfileToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.editProfileToolStripMenuItem.Text = "Edit Profiles";
            this.editProfileToolStripMenuItem.Click += new System.EventHandler(this.editProfileToolStripMenuItem_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Algerian", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(788, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(83, 33);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnWebBrowser
            // 
            this.btnWebBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWebBrowser.BackColor = System.Drawing.Color.Transparent;
            this.btnWebBrowser.FlatAppearance.BorderSize = 0;
            this.btnWebBrowser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWebBrowser.Font = new System.Drawing.Font("Algerian", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWebBrowser.ForeColor = System.Drawing.Color.Red;
            this.btnWebBrowser.Location = new System.Drawing.Point(591, 143);
            this.btnWebBrowser.Name = "btnWebBrowser";
            this.btnWebBrowser.Size = new System.Drawing.Size(280, 33);
            this.btnWebBrowser.TabIndex = 5;
            this.btnWebBrowser.Text = "Web Browser";
            this.btnWebBrowser.UseVisualStyleBackColor = false;
            this.btnWebBrowser.Click += new System.EventHandler(this.btnWebBrowser_Click);
            // 
            // lblClock
            // 
            this.lblClock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblClock.AutoSize = true;
            this.lblClock.BackColor = System.Drawing.Color.Transparent;
            this.lblClock.Font = new System.Drawing.Font("Algerian", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClock.ForeColor = System.Drawing.Color.Red;
            this.lblClock.Location = new System.Drawing.Point(12, 473);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(124, 31);
            this.lblClock.TabIndex = 0;
            this.lblClock.Text = "00:00:00";
            // 
            // Clock
            // 
            this.Clock.Interval = 1000;
            this.Clock.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnFull
            // 
            this.btnFull.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFull.BackColor = System.Drawing.Color.Transparent;
            this.btnFull.FlatAppearance.BorderSize = 0;
            this.btnFull.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFull.Font = new System.Drawing.Font("Algerian", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFull.ForeColor = System.Drawing.Color.Red;
            this.btnFull.Location = new System.Drawing.Point(611, 12);
            this.btnFull.Name = "btnFull";
            this.btnFull.Size = new System.Drawing.Size(171, 33);
            this.btnFull.TabIndex = 5;
            this.btnFull.Text = "Fullscreen";
            this.btnFull.UseVisualStyleBackColor = false;
            this.btnFull.Click += new System.EventHandler(this.btnFull_Click);
            // 
            // btnMini
            // 
            this.btnMini.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMini.BackColor = System.Drawing.Color.Transparent;
            this.btnMini.FlatAppearance.BorderSize = 0;
            this.btnMini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMini.Font = new System.Drawing.Font("Algerian", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMini.ForeColor = System.Drawing.Color.Red;
            this.btnMini.Location = new System.Drawing.Point(459, 12);
            this.btnMini.Name = "btnMini";
            this.btnMini.Size = new System.Drawing.Size(146, 33);
            this.btnMini.TabIndex = 5;
            this.btnMini.Text = "Minimize";
            this.btnMini.UseVisualStyleBackColor = false;
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnYMp4D
            // 
            this.btnYMp4D.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYMp4D.BackColor = System.Drawing.Color.Transparent;
            this.btnYMp4D.FlatAppearance.BorderSize = 0;
            this.btnYMp4D.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnYMp4D.Font = new System.Drawing.Font("Algerian", 12F);
            this.btnYMp4D.ForeColor = System.Drawing.Color.Red;
            this.btnYMp4D.Location = new System.Drawing.Point(522, 65);
            this.btnYMp4D.Name = "btnYMp4D";
            this.btnYMp4D.Size = new System.Drawing.Size(349, 33);
            this.btnYMp4D.TabIndex = 7;
            this.btnYMp4D.Text = "Youtube Mp4 Downloader";
            this.btnYMp4D.UseVisualStyleBackColor = false;
            this.btnYMp4D.Click += new System.EventHandler(this.btnYMp4D_Click);
            // 
            // btnMp
            // 
            this.btnMp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMp.BackColor = System.Drawing.Color.Transparent;
            this.btnMp.FlatAppearance.BorderSize = 0;
            this.btnMp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMp.Font = new System.Drawing.Font("Algerian", 12F);
            this.btnMp.ForeColor = System.Drawing.Color.Red;
            this.btnMp.Location = new System.Drawing.Point(591, 104);
            this.btnMp.Name = "btnMp";
            this.btnMp.Size = new System.Drawing.Size(280, 33);
            this.btnMp.TabIndex = 8;
            this.btnMp.Text = "Mp3 Player";
            this.btnMp.UseVisualStyleBackColor = false;
            this.btnMp.Click += new System.EventHandler(this.btnMp_Click);
            // 
            // MainBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(883, 513);
            this.Controls.Add(this.btnMp);
            this.Controls.Add(this.btnYMp4D);
            this.Controls.Add(this.btnMini);
            this.Controls.Add(this.btnFull);
            this.Controls.Add(this.btnWebBrowser);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblClock);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mnStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainBrowser";
            this.Text = "MainBrowser";
            this.Load += new System.EventHandler(this.MainBrowser_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainBrowser_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainBrowser_MouseMove);
            this.mnStrip.ResumeLayout(false);
            this.mnStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip mnStrip;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editProfileToolStripMenuItem;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnWebBrowser;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.Timer Clock;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btnFull;
        private System.Windows.Forms.Button btnMini;
        private System.Windows.Forms.Button btnYMp4D;
        private System.Windows.Forms.Button btnMp;
    }
}