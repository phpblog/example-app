﻿namespace Example_App
{
    partial class YtMp4Down
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YtMp4Down));
            this.txtURL = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblURL = new System.Windows.Forms.Label();
            this.btnDownload = new System.Windows.Forms.Button();
            this.btnExi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(80, 66);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(640, 22);
            this.txtURL.TabIndex = 1;
            this.txtURL.TextChanged += new System.EventHandler(this.txtURL_TextChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Font = new System.Drawing.Font("Algerian", 12F);
            this.lblStatus.ForeColor = System.Drawing.Color.White;
            this.lblStatus.Location = new System.Drawing.Point(279, 134);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(98, 22);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "Status...";
            // 
            // lblURL
            // 
            this.lblURL.AutoSize = true;
            this.lblURL.BackColor = System.Drawing.Color.Transparent;
            this.lblURL.Font = new System.Drawing.Font("Algerian", 12F);
            this.lblURL.ForeColor = System.Drawing.Color.White;
            this.lblURL.Location = new System.Drawing.Point(23, 66);
            this.lblURL.Name = "lblURL";
            this.lblURL.Size = new System.Drawing.Size(51, 22);
            this.lblURL.TabIndex = 0;
            this.lblURL.Text = "URL:";
            // 
            // btnDownload
            // 
            this.btnDownload.BackColor = System.Drawing.Color.Transparent;
            this.btnDownload.FlatAppearance.BorderSize = 0;
            this.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownload.Font = new System.Drawing.Font("Algerian", 12F);
            this.btnDownload.ForeColor = System.Drawing.Color.White;
            this.btnDownload.Location = new System.Drawing.Point(264, 94);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(148, 37);
            this.btnDownload.TabIndex = 2;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // btnExi
            // 
            this.btnExi.BackColor = System.Drawing.Color.Transparent;
            this.btnExi.FlatAppearance.BorderSize = 0;
            this.btnExi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExi.Font = new System.Drawing.Font("Algerian", 12F);
            this.btnExi.ForeColor = System.Drawing.Color.White;
            this.btnExi.Location = new System.Drawing.Point(645, 12);
            this.btnExi.Name = "btnExi";
            this.btnExi.Size = new System.Drawing.Size(75, 34);
            this.btnExi.TabIndex = 3;
            this.btnExi.Text = "Exit";
            this.btnExi.UseVisualStyleBackColor = false;
            this.btnExi.Click += new System.EventHandler(this.btnExi_Click);
            // 
            // YtMp4Down
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(732, 194);
            this.Controls.Add(this.btnExi);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.lblURL);
            this.Controls.Add(this.lblStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "YtMp4Down";
            this.Text = "Youtube Mp4 Downloader";
            this.Load += new System.EventHandler(this.YtMp4Down_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.YtMp4Down_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.YtMp4Down_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblURL;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnExi;
    }
}