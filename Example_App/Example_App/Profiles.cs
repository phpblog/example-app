﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example_App
{
    public partial class Profiles : Form
    {
        private Class sr;

        public Profiles()
        {
            InitializeComponent();
            Class.InitializeDb();

            ListV.FullRowSelect = true;
        }

        #region Buttons
        private void LoadAll()
        {
            List<Class> cl = Class.GetSources();

            ListV.Items.Clear();

            foreach (Class s in cl)
            {
                ListViewItem item = new ListViewItem(new String[] { s.Username.ToString(), s.Password.ToString() });
                item.Tag = s;

                ListV.Items.Add(item);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Profiles_Load(object sender, EventArgs e)
        {

        }

        private void btnLoadAll_Click(object sender, EventArgs e)
        {
            LoadAll();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string u = txtu.Text;
            string p = txtp.Text;

            if (String.IsNullOrEmpty(u) || String.IsNullOrEmpty(p))
            {
                MessageBox.Show("It's empty!");
                return;
            }

            sr = Class.Insert(u, p);

            LoadAll();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            String u = txtu.Text;
            String p = txtp.Text;

            if (String.IsNullOrEmpty(u) || String.IsNullOrEmpty(p))
            {
                MessageBox.Show("It's empty!");
                return;
            }
            sr.Update(u, p);

            LoadAll();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (sr == null)
            {
                MessageBox.Show("No item's selected!");
                return;
            }
            sr.Delete();

            LoadAll();
        }
        #endregion

        #region ListView
        private void ListV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListV.SelectedItems.Count > 0)
            {
                ListViewItem item = ListV.SelectedItems[0];
                sr = (Class)item.Tag;

                String u = sr.Username;
                String p = sr.Password;

                txtu.Text = u.ToString();
                txtp.Text = p.ToString();
            }
        }

        #endregion

        #region Draggable
        private void Profiles_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;

            }
        }
        Point lastPoint;
        private void Profiles_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        #endregion
    }
}
