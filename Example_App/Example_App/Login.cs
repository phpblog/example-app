﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MySql.Data.MySqlClient;

namespace Example_App
{
    public partial class Login : Form
    {        
        public Login()
        {
            InitializeComponent();
        }

        #region Buttons & Form
        private void btnLogIn_Click(object sender, EventArgs e)
        {
            string user = txtUsername.Text;
            string pass = txtPassword.Text;
            MySqlConnection con = new MySqlConnection("server=localhost;user id=root;database=exampledatabase;SslMode=none;");
            MySqlDataAdapter adp = new MySqlDataAdapter("select count(*) from login where username='" + txtUsername.Text + "' and password='" + txtPassword.Text + "'", con);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            if (dt.Rows[0][0].ToString() == "1")
            {
                Thread th = new Thread(OpenMain);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid Username or Password\nPlease try Again.");
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void llblRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Thread th = new Thread(OpenRegister);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
            this.Close();
        }
        #endregion

        #region OpenForms
        private void OpenRegister(object obj)
        {
            Application.Run(new CreateNew());
        }

        private void OpenMain(object obj)
        {
            Application.Run(new MainBrowser());
        }
        #endregion

        #region Drag and Drop
        Point lastPoint;
        private void Login_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        #endregion
    }
}
