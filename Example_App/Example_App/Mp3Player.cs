﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example_App
{
    public partial class Mp3Player : Form
    {
        private MusicPlay MusicPlay = new MusicPlay();
        public Mp3Player()
        {
            InitializeComponent();
        }

        #region Buttons
        private void btnClose_Click(object sender, EventArgs e)
        {
            MusicPlay.stop();
            this.Close();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            MusicPlay.play();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            MusicPlay.stop();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Filter = "MP3|*.mp3";
                ofd.Multiselect = true;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    MusicPlay.open(ofd.FileName);
                }
            }
        }
        #endregion

        #region Draggable
        private void Mp3Player_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;

            }
        }
        Point lastPoint;
        private void Mp3Player_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        #endregion
    }
}
