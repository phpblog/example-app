﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Threading;

namespace Example_App
{
    public partial class CreateNew : Form
    {
        private Class cl;
        public CreateNew()
        {
            InitializeComponent();
            Class.InitializeDb();
        }

        #region Buttons & Form
        private void btnCreate_Click(object sender, EventArgs e)
        {
            string user = txtUsername.Text;
            string pass = txtPassword.Text;
            MySqlConnection con = new MySqlConnection("server=localhost;user id=root;database=exampledatabase;SslMode=none;");
            MySqlDataAdapter adp = new MySqlDataAdapter("select count(*) from login where username='" + txtUsername.Text + "' and password='" + txtPassword.Text + "'", con);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            if (dt.Rows[0][0].ToString() != "1" & !String.IsNullOrWhiteSpace(txtUsername.Text) & !String.IsNullOrWhiteSpace(txtPassword.Text))
            {
                cl = Class.Register(user, pass);
                Thread th = new Thread(OpenLogin);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();
                this.Close();
                MessageBox.Show("Username and password created.");
            }
            else
            {
                MessageBox.Show("This Username is already taken or the username/password is not filled.");
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(OpenLogin);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
            this.Close();
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CreateNew_Load(object sender, EventArgs e)
        {
            
        }
        #endregion
        
        #region OpenForm
        private void OpenLogin(object obj)
        {
             Application.Run(new Login());
        }
        #endregion
    }
}
