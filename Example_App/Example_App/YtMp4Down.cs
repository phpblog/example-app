﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VideoLibrary;

namespace Example_App
{
    public partial class YtMp4Down : Form
    {
        public YtMp4Down()
        {
            InitializeComponent();
        }

        #region Button

        private async void btnDownload_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog() { Description = "Select your path." })
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    var youtube = YouTube.Default;
                    lblStatus.Text = "Downloading...";
                    var video = await youtube.GetVideoAsync(txtURL.Text);
                    File.WriteAllBytes(Path.Combine(fbd.SelectedPath, video.FullName), await video.GetBytesAsync());
                    lblStatus.Text = "Completed!";
                }
            }
        }

        private void btnExi_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Draggable
        private void YtMp4Down_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;

            }
        }

        Point lastPoint;

        private void YtMp4Down_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        #endregion

        private void txtURL_TextChanged(object sender, EventArgs e)
        {

        }

        private void YtMp4Down_Load(object sender, EventArgs e)
        {

        }
    }
}
