﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Example_App
{
    public partial class ZBrowser : Form
    {
        #region Declaration
        private Point imageLocation = new Point(13, 5);
        private Point imageHitArea = new Point(13, 2);
        Image CloseImage;
        #endregion
        public ZBrowser()
        {
            InitializeComponent();
        }
        #region Buttons & Form & tabControl
        private void btnForward_Click(object sender, EventArgs e)
        {
            WebBrowser web = tabControl.SelectedTab.Controls[0] as WebBrowser;
            if (web != null)
            {
                if (web.CanGoForward)
                {
                    web.GoForward();
                }
            }
        }

        private void btnPervious_Click(object sender, EventArgs e)
        {
            WebBrowser web = tabControl.SelectedTab.Controls[0] as WebBrowser;
            if (web != null)
            {
                if (web.CanGoBack)
                {
                    web.GoBack();
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {            
            WebBrowser web = tabControl.SelectedTab.Controls[0] as WebBrowser;
            if (web != null)
            {
                Dictionary<string, string> lstParams = new Dictionary<string, string>();
                lstParams["address"] = txtSearch.Text;
                web.Navigate("http://www.google.com/search?q=" + lstParams["address"]);
            }
        }

        private void btnNewTab_Click(object sender, EventArgs e)
        {
            TabPage tab = new TabPage();
            tab.Text = "New Tab";
            tabControl.Controls.Add(tab);
            tabControl.SelectTab(tabControl.TabCount - 1);
            webTab = new WebBrowser() { ScriptErrorsSuppressed = true };
            webTab.Parent = tab;
            webTab.Dock = DockStyle.Fill;
            webTab.Navigate("http://www.google.com");
            txtURL.Text = "http://www.google.com";
            webTab.DocumentCompleted += WebTab_DocumentCompleted;
        }

        private void WebTab_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            tabControl.SelectedTab.Text = webTab.DocumentTitle;
        }

        private void WebForm_Load(object sender, EventArgs e)
        {
            webBrowser.Navigate("http://www.google.com");
            webBrowser.DocumentCompleted += WebBrowser_DocumentCompleted;

            tabControl.DrawMode = TabDrawMode.OwnerDrawFixed;
            tabControl.DrawItem += TabControl_DrawItem;
            CloseImage = Example_App.Properties.Resources.close;
            tabControl.Padding = new Point(10, 3);


        }

        private void TabControl_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            try
            {
                Image img = new Bitmap(CloseImage);
                Rectangle r = e.Bounds;
                r = this.tabControl.GetTabRect(e.Index);
                r.Offset(2, 2);
                string title = this.tabControl.TabPages[e.Index].Text;
                Font f = this.Font;
                Brush titleBrush = new SolidBrush(Color.Black);
                e.Graphics.DrawString(title, f, titleBrush, new PointF(r.X, r.Y));

                if (tabControl.SelectedIndex <=1)
                {
                    e.Graphics.DrawImage(img, new Point(r.X + (this.tabControl.GetTabRect(e.Index).Width - imageLocation.X), imageLocation.Y));

                }
            }
            catch (Exception)
            {
                                
            }
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            tabControl.SelectedTab.Text = webBrowser.DocumentTitle;
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            WebBrowser web = tabControl.SelectedTab.Controls[0] as WebBrowser;
            if (web != null)
            {
                web.Navigate(txtURL.Text);
            }
        }
        WebBrowser webTab = null;

        private void txtURL_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtURL_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                WebBrowser web = tabControl.SelectedTab.Controls[0] as WebBrowser;
                if (web != null)
                {
                    web.Navigate(txtURL.Text);
                }
            }
        }

        private void tabControl_MouseClick(object sender, MouseEventArgs e)
        {
            TabControl tc = (TabControl)sender;
            Point p = e.Location;
            int TabWidth = 0;
            TabWidth = this.tabControl.GetTabRect(tc.SelectedIndex).Width - (imageHitArea.X);
            Rectangle r = this.tabControl.GetTabRect(tc.SelectedIndex);
            r.Offset(TabWidth, imageHitArea.Y);
            r.Width = 16;
            r.Height = 16;
            if (tabControl.SelectedIndex >= 1)
            {
                if (r.Contains(p))
                {
                    TabPage tab = (TabPage)tc.TabPages[tc.SelectedIndex];
                    tc.TabPages.Remove(tab);
                }
            }
        }

        #endregion

        #region Place holder text
        private void txtSearch_Enter(object sender, EventArgs e)
        {
            if (txtSearch.Text == "Google Search")
            {
                txtSearch.Text = "";
                txtSearch.ForeColor = Color.Black;
            }
        }

        private void txtSearch_Leave(object sender, EventArgs e)
        {
            if (txtSearch.Text == "")
            {
                txtSearch.Text = "Google Search";
                txtSearch.ForeColor = Color.Gray;
            }
        }
        #endregion
    }
}
