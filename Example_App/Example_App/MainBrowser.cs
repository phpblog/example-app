﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Example_App
{
    public partial class MainBrowser : Form
    {
        public MainBrowser()
        {
            InitializeComponent();
        }

        #region Form & Clock
        private void MainBrowser_Load(object sender, EventArgs e)
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new MethodInvoker(delegate ()
            {
                lblClock.Text = DateTime.Now.ToString("T");
            }));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblClock.Text = DateTime.Now.ToString("T");
        }

        #endregion

        #region Exit Fullscreen Minimize
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnFull_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
            {
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Normal;
                TopMost = false;
            }
            else
            {
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
                TopMost = true;
            }
        }

        private void btnMini_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        #endregion

        #region Draggable
        private void MainBrowser_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;

            }
        }
        Point lastPoint;
        private void MainBrowser_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        #endregion

        #region Buttons
        private void btnWebBrowser_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(OpenWeb);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
                
        private void btnYMp4D_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(OpenYMp4D);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
        
        private void btnMp_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(OpenMp3);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }
        
        private void editProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread th = new Thread(OpenProfiles);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private void OpenProfiles(object obj)
        {
            Application.Run(new Profiles());
        }

        private void OpenMp3(object obj)
        {
            Application.Run(new Mp3Player());
        }

        private void OpenWeb(object obj)
        {
            Application.Run(new ZBrowser());
        }

        private void OpenYMp4D(object obj)
        {
            Application.Run(new YtMp4Down());
        }
        #endregion
    }
}
